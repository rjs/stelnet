# Borland C (DOS) Makefile for stelnet

# Compiler directories and executable
INCLUDEDIR=e:\borlandc\include
LIBDIR=e:\borlandc\lib
CC=e:\borlandc\bin\bcc

# Define buffer sizes and the loop count here if needed
# Defaults: TIBLEN=512, SIBLEN=512, TOBLEN=8192, SOBLEN=8192,
# SOBLIMIT=512, LOOPCOUNT=100
#OPTIONS=-DTIBLEN=512 -DSIBLEN=512 -DTOBLEN=8192 -DSOBLEN=8192 \
#        -DSOBLIMIT=512 -DLOOPCOUNT=100

# Uncomment one
# Disable debugging
DEBUGOPTS=-v- -O2
# Enable debugging
#DEBUGOPTS=-v

# Uncomment one set
# Small memory model (should be enough)
CMODEL=-ms
CLIB=..\lib\wattcpsm.lib
# Large memory model (for large buffer sizes (>~60K total))
#CMODEL=-ml
#CLIB=..\lib\wattcplg.lib

CFLAGS= $(CMODEL) -r- $(DEBUGOPTS) -I$(INCLUDEDIR) -I..\include -L$(LIBDIR) $(OPTIONS)

stelnet.exe: stelnet.c makefile
        $(CC) $(CFLAGS) stelnet.c $(CLIB)

