#!/usr/bin/awk
# Sample awk script for calculating totals from stelnet.act
# Use with: awk -f account.awk <stelnet.act
BEGIN { FS=","; }
{
  if (sdate == 0) sdate = $1;
  t[$2] += $5; ib[$2] += $6; ob[$2] += $7;
  edate = $1;
}
END {
  print "- From " sdate " to " edate
  for (u in t) {
    print  u ": Time " t[u] " s, input " ib[u] " bytes, output " \
      ob[u] " bytes";
  }
}
